<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block_type/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	/** Blocks **/
	$dropdown_block = [
		'name' => 'dropdown-block',
		'title' => __( 'Dropdown Block', 'wsc' ),
		'description' => __( 'Creates a dropdown container; The content is folded into the dropdown title.', 'wsc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'wsc-blocks',
		'align' => 'center',
		'icon' => 'sort',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'drop', 'down', 'dropdown', 'accordion' ]
	];
	acf_register_block_type( $dropdown_block );

	$teal_button_block = [
		'name' => 'teal-border-button-block',
		'title' => __( 'Bordered Teal Button', 'wsc' ),
		'description' => __( 'Creates a teal colored bordered button.', 'wsc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'wsc-blocks',
		'align' => 'center',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'teal', 'green', 'bordered' ]
	];
	acf_register_block_type( $teal_button_block );

	$solid_teal_button_block = [
		'name' => 'solid-teal-button-block',
		'title' => __( 'Solid Teal Button', 'wsc' ),
		'description' => __( 'Creates a button with a teal background and white text.', 'wsc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'wsc-blocks',
		'align' => 'center',
		'icon' => 'editor-removeformatting',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'button', 'anchor', 'teal', 'solid', 'green' ]
	];
	acf_register_block_type( $solid_teal_button_block );

	$cursive_text_block = [
		'name' => 'cursive-text-block',
		'title' => __( 'Cursive Text Block', 'wsc' ),
		'description' => __( 'Creates a H2 sized cursive text block.', 'wsc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'wsc-blocks',
		'align' => 'center',
		'icon' => 'edit',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'cursive', 'text', 'teal' ]
	];
	acf_register_block_type( $cursive_text_block );

	$q_and_a_block = [
		'name' => 'qa-block',
		'title' => __( 'QA Block', 'wsc' ),
		'description' => __( 'Creates a special block with mixed fonts for "Q and A" text. NOTE: Header for Q and A is auto-added for you.', 'wsc' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'wsc-blocks',
		'align' => 'left',
		'icon' => 'flag',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'faqs', 'q', 'a', 'and', 'question', 'answer' ]
	];
	acf_register_block_type( $q_and_a_block );
endif;