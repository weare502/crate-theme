<?php
// setup the post type
$labels = [
	'name'               => __( 'Farmer Stories', 'wsc' ),
	'singular_name'      => __( 'Story', 'wsc' ),
	'add_new'            => _x( 'Add Story', 'wsc', 'wsc' ),
	'add_new_item'       => __( 'Add Story', 'wsc' ),
	'edit_item'          => __( 'Edit Story', 'wsc' ),
	'new_item'           => __( 'New Story', 'wsc' ),
	'view_item'          => __( 'View Story', 'wsc' ),
	'search_items'       => __( 'Search Stories', 'wsc' ),
	'not_found'          => __( 'No Stories found', 'wsc' ),
	'not_found_in_trash' => __( 'No Stories found in Trash', 'wsc' ),
	'parent_item_colon'  => __( 'Parent Story:', 'wsc' ),
	'menu_name'          => __( 'Farmer Stories', 'wsc' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_icon'           => 'dashicons-book-alt',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'thumbnail', 'editor' ],
];
register_post_type( 'story', $args );