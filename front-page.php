<?php
/**
 * Template Name: Home
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// farmer story posts (Only the 2 most recent are grabbed / shown)
$farmer_stories = [
	'post_type' => 'story',
	'posts_per_page' => '2',
	'orderby' => 'date',
	'order' => 'ASC'
];

$context['farmer_stories'] = Timber::get_posts( $farmer_stories );

$templates = [ 'front-page.twig' ];

Timber::render( $templates, $context );