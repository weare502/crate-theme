<?php

// change 'views' directory to 'templates'
Timber::$locations = __DIR__ . '/templates';

class WSCSite extends TimberSite {

	function __construct() {
		// Action Hooks //
		add_action( 'after_setup_theme', [ $this, 'after_setup_theme' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
		add_action( 'enqueue_block_assets', [ $this, 'backend_frontend_styles' ] );
		add_action( 'admin_head', [ $this, 'admin_head_css' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu_cleanup'] );
		add_action( 'init', [ $this, 'register_post_types' ] );
		add_action( 'acf/init', [ $this, 'render_custom_acf_blocks' ] );

		// Filter Hooks //
		add_filter( 'timber_context', [ $this, 'add_to_context' ] );
		add_filter( 'gform_enable_field_label_visibility_settings', [ $this, '__return_true' ] );
		add_filter( 'block_categories', [ $this, 'wsc_block_category' ], 10, 2 );

		// Comment Column Removal //
		add_filter( 'manage_edit-page_columns', [ $this, 'disable_admin_columns' ] );

		parent::__construct();
	}

	// hide admin area annoyances
	function admin_head_css() {
		?><style type="text/css">
			#wp-admin-bar-comments { display: none !important; } /* Never Used */
			.update-nag { display: none !important; } /* Update Popup */
			.mwp-notice-container { display: none !important; } /* ManageWP popup */
		</style><?php
	}

	function enqueue_scripts() {
		$version = '20000020';
		wp_enqueue_style( 'wsc-css', get_stylesheet_directory_uri() . '/style-dist.css', [], $version );
		wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/static/js/slick.min.js', ['jquery'], $version );
		wp_enqueue_script( 'wsc-js', get_template_directory_uri() . '/static/js/site-dist.js', ['jquery', 'slick-js'], $version );
	}

	// Uses the 'enqueue_block_assets' hook
	function backend_frontend_styles() {
		wp_enqueue_style( 'blocks-css', get_stylesheet_directory_uri() . '/block-style-dist.css' );
	}

	// Custom Timber context helper functions
	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['date'] = date('F j, Y');
		$context['date_year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['home_url'] = home_url('/');
		$context['is_front_page'] = is_front_page();
		$context['get_url'] = $_SERVER['REQUEST_URI'];

		return $context;
	}

	// Menus / Theme Support / ACF Options Page
	function after_setup_theme() {
		register_nav_menu( 'primary', 'Site Navigation' );
		register_nav_menu( 'post-nav', 'Site Navigation - Post Page' );
		register_nav_menu( 'footer-nav', 'Footer Menu' );

		add_theme_support( 'menus' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'post-thumbnails' );

		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page([
				'page_title' => 'Global Site Data',
				'menu_title' => 'Global Site Data',
				'capability' => 'edit_posts',
				'redirect' => false,
				'updated_message' => 'Global Options Updated!'
			]);
		}
	}

	// registers and renders our custom acf blocks
	function render_custom_acf_blocks() {
		require 'acf-block-functions.php';
	}

	// creates a custom category for our theme-specific blocks
	function wsc_block_category( $categories, $post ) {
		return array_merge( $categories,
			[[
				'slug' => 'wsc-blocks',
				'title' => 'WSC Blocks'
			],
		]);
	}

	// get rid of clutter
	function disable_admin_columns( $columns ) {
		unset( $columns['comments'] );
		return $columns;
	}

	function admin_menu_cleanup() {
		remove_menu_page( 'edit.php' ); // Posts
		remove_menu_page( 'edit-comments.php' ); // Comments
	}

	// add cpts here
	function register_post_types() {
		include_once('custom-post-types/post-type-story.php');
	}
} // End of WSCSite class

new WSCSite();

// main site nav
function wsc_render_primary_menu() {
	wp_nav_menu([
		'theme_location' => 'primary',
		'container' => false,
		'menu_id' => 'primary-menu'
	]);
}

// main site nav clone for jump links back to homepage
function wsc_render_single_post_clone() {
	wp_nav_menu([
		'theme_location' => 'post-nav',
		'container' => false,
		'menu_id' => 'primary-menu'
	]);
}

function wsc_render_footer_menu() {
	wp_nav_menu([
		'theme_location' => 'footer-nav',
		'container' => 'false',
		'menu_id' => 'footer-menu'
	]);
}

// run if  _wp_page_template  is not empty (custom template is used)
// for the Default Template it will be empty. (default is used when no template is set)
// Code Courtesy of: Bill Erickson
function ea_disable_editor( $id = false ) {
	$excluded_templates = [
		'front-page.php'
	];

	if( empty( $id ) )
		return false;

	$id = intval( $id );
	$template = get_page_template_slug( $id );

	return in_array( $template, $excluded_templates );
}

function ea_disable_gutenberg( $can_edit, $post_type ) {
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;

	if( ea_disable_editor( $_GET['post'] ) )
		$can_edit = false;

	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );

// move our ACF Options Page (Global Site Data) below the Dashboard tab
function custom_menu_order( $menu_ord ) {  
    if ( ! $menu_ord ) {
		return true;
	}

    $menu = 'acf-options-global-site-data';

    // remove from current menu
    $menu_ord = array_diff( $menu_ord, [$menu] );

    // append after index [0]
    array_splice( $menu_ord, 1, 0, [$menu] );

    return $menu_ord;
}
add_filter( 'custom_menu_order', 'custom_menu_order' );
add_filter( 'menu_order', 'custom_menu_order' );

// Shows how many Boxes are left on the Crate Signup form
add_filter( 'gform_shortcode_entries_left', 'gwiz_entries_left_shortcode', 10, 2 );
function gwiz_entries_left_shortcode( $output, $atts ) {
	extract( shortcode_atts([
		'id' => false,
		'format' => false
	], $atts ) );

	if( ! $id )
		return '';

	$form = RGFormsModel::get_form_meta( $id );

	if( ! rgar( $form, 'limitEntries' ) || ! rgar( $form, 'limitEntriesCount' ) )
		return '';

	$entry_count = RGFormsModel::get_lead_count( $form['id'], '', null, null, null, null, 'active' );
	$entries_left = rgar( $form, 'limitEntriesCount' ) - $entry_count;
	$output = $entries_left;

	if( $format ) {
		$format = $format == 'decimal' ? '.' : ',';
		$output = number_format( $entries_left, 0, false, $format );
	}

	return $entries_left > 0 ? $output : 0;
}