(function($) {
    $(document).ready(function() {
		let $body = $('body');

		// mobile menu open/close functions
		$(function siteNavigation() {
			$('#menu-toggle').click(function() {
				$('.x-bar').toggleClass('x-bar-active');
				$('#primary-menu').fadeToggle(550);
			});
		});

		// onscroll event -  $(window).bind('ready scroll', () => { stickyNav(); });
		$(function stickyNav() {
			var $nav = $('#sticky-nav');
			var pos = $nav.offset().top;

			// once object is in view and we would scroll past it, add the sticky class to it (fixed to top of window)
			$(window).scroll(function() {
				var scrollPos = $(this).scrollTop(); // track scrolling from top position

				if(scrollPos > pos) { // add class if we would scroll past the object (nav)
					$nav.addClass('sticky');
				} else {
					$nav.removeClass('sticky');
				}
			});
		});

		// dropdown block - controls and aria events for screenreaders
		$(function dropDownBlock() {
			$('.dropdown-content').each(function() {
				$(this).hide();
			});

			$('.dropdown-block-title').click(function() {
				var $this = $(this);
				// fires on first click (content is expanded)
				if( $this.hasClass('target') ) {
					$this.removeClass('target');
					$this.next('.dropdown-content').slideToggle(700);
					$this.attr('aria-pressed', 'true');
					$this.next('.dropdown-content').attr('aria-expanded', 'true');
				} else {
					// fires on second click (content is closed)
					$this.next('.dropdown-content:first').slideToggle(700, function() {
						$this.prev('.dropdown-block-title').addClass('target');
						$this.prev('.dropdown-block-title').attr('aria-pressed', 'false');
						$this.attr('aria-expanded', 'false');
					});
				}
				// always fire
				$(this).toggleClass('chevron-rotate');
			});
		});

		// smooth scroll function
		if( $body.hasClass('home') ) {
			$(function smoothScroll() {
				$('a[href*="#"]').not('[href="#"]').not('[href="#0"]').not('.home-header a[href="#get-current-crate"]').click(function(event) {

					// close the menu and revert the x-bar animation (resets to initial state without bubbling)
					if($body.width() < 900) {
						$('.x-bar').toggleClass('x-bar-active');
						$('#primary-menu').fadeToggle(550);
					}

					if( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname ) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

						if(target.length) {
							// Only prevent default if animation is actually gonna happen
							event.preventDefault();
							$('html, body').animate({ scrollTop: target.offset().top }, 1000, function() {
								var $target = $(target);

								if ($target.is(":focus")) {
									return false;
								} else {
									$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
								}
							}); // end of html/body animate
						} // end of length check
					} // end path check
				}); // end scroll click event

				// for the homepage bouncy arrow
				$('.home-header a[href="#get-current-crate"]').click(function(event) {
					if( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname ) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

						if(target.length) {
							// Only prevent default if animation is actually gonna happen
							event.preventDefault();
							$('html, body').animate({ scrollTop: target.offset().top }, 1000, function() {
								var $target = $(target);

								if ($target.is(":focus")) {
									return false;
								} else {
									$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
								}
							}); // end of html/body animate
						} // end of length check
					} // end path check
				}); // end scroll click event
			});

			// breakpoints work as "less than this number, do x"
			$(function loadSlider() {
				$('.slider-wrapper').slick({
					slidesToShow: 6,
					slidesToScroll: 1,
					centerMode: true,
					autoplay: true,
					autoplaySpeed: 4000,
					pauseOnHover: true,
					infinite: true,
					arrows: false,
					dots: false,
					draggable: true,
					responsive: [
						{ breakpoint: 1250,
							settings: { slidesToShow: 4 }
						},

						{ breakpoint: 850,
							settings: { slidesToShow: 3 }
						},

						{ breakpoint: 768,
							settings: { slidesToShow: 2 }
						},

						{ breakpoint: 480,
							settings: { slidesToShow: 1 }
						}
					]
				});
			});
		} // end of $body class check

	}); // end Document.Ready
})(jQuery);